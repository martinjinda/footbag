<div id="content" class="container-fluid">
	<div id="venues-maps" class="container">

		<h2 class="main-headline">Venues &amp maps</h2>
		<div class="row">
			<div id="map" class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h3>Event map</h3>
			</div>
			<div class="col-sm-12 col-md-12 photo-block map-block">
				<iframe src="https://www.google.com/maps/d/u/0/embed?mid=zffPHZacNxV4.kzcISuVPEGFk" height="360"></iframe>
			</div>
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h4>Mestska Športová Hala</h4>
				<p>The <strong>City Sports Hall</strong> will host all the freestyle events and the showcased finals of both net and freestyle in the last two days of worlds schedule. We will provide a special flooring suitable for freestyle competition, warm-up, and casual shred session. Showcased net finals will have 2 mobile net courts installed. The venue has stands for spectators that we expect to show up in large numbers during our finals.</p>
			</div>
			<div class="col-sm-12 col-md-12 photo-block">
				<img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/hala1.jpg" alt="Mestska Športová Hala">
			</div>
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<address>
					<strong>Address:</strong> Rybníková, 917 01 Trnava, Slovensko
				</address>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h4>Empire Hall</h4>
				<p>Located 400 m from the City Sport Hall and adjacent to the Empire Hotel, this modern sport hall will have 12 mobile net courts installed where all the preliminary net games take place. A restaurant is located above the fields so you can watch net games and enjoy your meal. Showcase finals will be played in the City Sports Hall.</p>
			</div>
			<div class="col-sm-12 col-md-12 photo-block">
				<img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/hala-empire.jpg" alt="Empire Hall">
			</div>
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<address>
					<strong>Address:</strong> Jána Hajdóczyho 7736/11, 917 01 Trnava, Slovensko
				</address>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h4>Slavia gym</h4>
				<p>A backup gym with 4 net courts on a wooden floor.</p>
			</div>
		</div>

	</div>
</div>