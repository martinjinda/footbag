<div id="content" class="container-fluid">
	<div id="disciplines" class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h2 class="main-headline">Footbag disciplines</h2>
				<p>Footbag is a group of sports based on kicking a small round bag or ball with the legs of the player. In its today’s form Footbag was invented in 1972 in North America and since that time it’s been evolving into a popular sport played all over the world. In Poland Footbag has probably one of the longest histories because in its basic form, it was popular from the beginning of the 20th century as a game called “Zośka”. Due to this fact Footbag is generally known by the Polish society as an extreme form of “Zośka”, what is a popular Polish female name. Professional Footbag consists of two main disciplines: Footbag Net and Footbag Freestyle.</p>
			</div>
		</div>

		<div id="discipline-net" class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h3>Footbag Net</h3>
				<p>Footbag net is a singles or doubles court game where players use their feet only (below the knee) to kick the footbag over the net. The size of the court and the height of the net are the same as in badminton. The rules for doubles net are a lot like volleyball: players are allowed three kicks per side, and must alternate kicks. In singles, however, players are only allowed two kicks per side.</p>
				<p>Playing strategy is much similar to beach volley. Offence is basically done by setting the bag up and hitting it down to the opponents’ side. Good offensive players hit the bag hard while simultaneously directing it with good accuracy to an empty spot. Defence is done by either blocking the opponents’ hits over the net or moving rapidly and “digging” the bag up, preparing for counter-attack. All of this is done with feet only!</p>
				<p>The best footbag net players are exceptional athletes. Playing on the highest level requires extreme body coordination, quickness, flexibility, endurance, etc. And these are only the physical requirements. Good game reading skills, mind control and focus are also extremely important qualities.</p>
			</div>
		</div>

		<div class="col-sm-12 col-md-12 photo-block">
			<img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/disciplines/net.jpg" alt="Footbag Net">
		</div>

		<div id="discipline-freestyle" class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h3>Footbag Freestyle</h3>
				<p>Footbag Freestyle is a sport in which the object is to perform tricks with the bag. It has evolved into a very technical competitive discipline. Players compete by choreographing routines, performing very difficult combinations of footbag tricks to music (not unlike in figure skating). Routines are judged along four axes: presentation, difficulty, variety, and execution.</p>
				<p>Other forms of competitive and non-competitive freestyle exist, including “shredding” in which play­ers attempt to perform as many tricks in a row as possible, of a given add-value or above. They also attempt to show variety in their strings, along several dimensions.</p>
				<p>Each move or trick in freestyle foot­bag has a determinable difficulty rating. The difficulty of each move is added, and the total is divided by the number of attempted and com­peted moves in the routine to deter­mine the average difficulty. Difficulty is measured in “adds”, which repre­sent additional levels of difficulty beyond the basic moves.</p>
			</div>
		</div>
		
		<div class="col-sm-12 col-md-12 photo-block">
			<img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/disciplines/freestyle.jpg" alt="Footbag Net">
		</div>

		<div id="discipline-golf" class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h3>Footbag Golf</h3>
				<p>Footbag Golf is a casual game based on the rules of classic golf with two main diferences: instead of golf clubs players use theirs legs and golf balls are obviously replaced by footbags. This discipline is enjoyable for begginers because the game isn’t hard, rules are clear and the best place for footbag golf course would be a park in your town!</p>
			</div>
		</div>

		<div class="col-sm-12 col-md-12 photo-block">
			<img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/disciplines/golf.jpg" alt="Footbag Net">
		</div>

	</div>
</div>