<div id="content" class="container-fluid">
	<div id="event-team" class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h2 class="main-headline">Event team</h2>
				<h3>Czech Footbag Association + IFPA</h3>
				<ul class="list list-normal list-indent">
					<li><strong>Tournament Director:</strong> <a href="mailto:dexter@footbag.org">Jan Struž</a>, +420 603 435 677</a></li>
					<li><strong>Tournament Co-Director:</strong> Peter Bročka</li>
					<li><strong>Freestyle Director:</strong> <a href="mailto:moglum@footbag.org">Vojta Polák</a></li>
					<li><strong>Net Director:</strong> Tuomas Kärki</li>
					<li><strong>Accommodation coordinator:</strong> <a href="mailto:trnava16accomodation@gmail.com">Viatcheslav Sidorin</a></li>
					<li><strong>Website:</strong> Michal Daněk, <a href="mailto:martin@jinda.cz">Martin Jinda</a></li>
					<li><strong>Video Contest:</strong> Vojta Janoušek</li>
					<li><strong>Production:</strong> Dan Kubík, Lukáš Škoda</li>
					<li><strong>PR:</strong> TBA</li>
				</ul>
			</div>
		</div>
	</div>
</div>