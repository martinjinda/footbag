<div id="content" class="container-fluid">
  <div id="hall-of-fame" class="container">
    <h2 class="main-headline">Hall of Fame</h2>
    <div class="row">
      <div id="city" class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <p>The <a href="http://www.footbag.org/groups/list/hof" target="_blank">Footbag Hall of Fame</a> (often abbreviated “HoF”) is footbag’s group of honored people that have contributed greatly to footbag’s growth since 1972, when it was first invented. The current membership is kept up to date on the footbag.org group (so check there to see the current member list).

          The <a href="http://www.footbag.org/groups/list/hof" target="_blank">Footbag Hall of Fame</a> (often abbreviated “HoF”) is footbag’s group of honored people that have contributed greatly to footbag’s growth since 1972, when it was first invented. The current membership is kept up to date on the footbag.org group (so check there to see the current member list).
        </p>
      </div>
      <div id="history" class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <h4>History</h4>
        <p>John Stalberger met Mike Marshall by chance in 1972. John liked Mike’s kicking idea of using a homemade sack filled with beans. From his background in football, baseball, and physical therapy, John took the homemade sack and turned it into a specifically designed object. A “Hacky Sack,” for playing a new game called “Footbag.” The first round Footbag was a 2-piece design, similar to a baseball, and the plastic pellets worked ideally for giving the Footbag new energy with every kick. John then began figuring out the best coordinated athletic movements for this new game.</p>
        <p>Emphasis was put on the study of the proper kicking techniques, and the correct use of the upper body for counter-balancing. Equal use of both feet to block the angles of the Footbags flight would also help control the Footbag and avoid injuries. These new techniques helped develop greater levels of concentration, coordination, flexibility, and strength. All of which are needed for any sport.</p>
        <p>Not to mention it was fun and could be played by all ages, and skill levels! It was also realized that this new game could be used as an exercise by any athlete as a warm up, and/or basic training. They also envisioned that a sport could be created from this new game.</p>
        <p>After Mike suddenly and tragically died in his sleep in 1975, John continued on with the dream. He started a Footbag manufacturing company called “The National Hacky Sack Company.” This was the first company to start organizing and teaching the footbag concept to schools in Oregon.</p>
        <p>In 1977 his friends helped him form a player’s association for the sport, named “The National Hacky Sack Association.” (The NHSA) The first ever Footbag tournament was held in Oregon City, Oregon.</p>
        <p>Today, Mike and John’s dreams have been realized. Footbag has become a full-fledged sport that is played worldwide. Their dedication continues to be the inspiration for the game and sport of Footbag.</p>
      </div>
    </div>
  </div>
</div>