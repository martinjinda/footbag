<div id="content" class="container-fluid">
	<div id="schedule" class="container">
		<div class="row">
			<h2 class="main-headline">Schedule</h2>
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<p>A full, final schedule is in every player’s pack. Check notice boards and listen for any changes during the event.</p>

				<!-- Nav tabs -->
				<ul class="nav nav-tabs no-bordered" role="tablist">
					<li role="presentation" class="active"><a href="#freestyle-events" aria-controls="freestyle-events" role="tab" data-toggle="tab">Freestyle events</a></li>
					<li role="presentation"><a href="#net-events" aria-controls="net-events" role="tab" data-toggle="tab">Net events</a></li>
					<li role="presentation"><a href="#finals" aria-controls="finals" role="tab" data-toggle="tab">Finals</a></li>
					<li role="presentation"><a href="#other-events" aria-controls="other-events" role="tab" data-toggle="tab">Other events</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="freestyle-events">
						<h3>Freestyle events</h3>
						<table class="table table-hover">
							<thead>
							<tr>
								<th colspan="2">Monday, August 1</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>11:00</td>
								<td>Circle Judges Meeting</td>
							</tr>
							<tr>
								<td>12:00</td>
								<td>Players’ Meeting & Warm Up</td>
							</tr>
							<tr>
								<td>12:30</td>
								<td>Open & Women’s Circle Contest Qualifications</td>
							</tr>
							<tr>
								<td>17:00</td>
								<td>Routines Judges Meeting & Judging Workshop</td>
							</tr>
							</tbody>
							<thead>
							<tr>
								<th colspan="2">Tuesday, August 2</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>11.00</td>
								<td>Singles Routines Qualifications (all categories)</td>
							</tr>
							<tr>
								<td>15.30</td>
								<td>Open Shred 30 1st Round</td>
							</tr>
							<tr>
								<td>16.30</td>
								<td>Women’s & Intermediate Shred 30 Finals</td>
							</tr>
							</tbody>
							<thead>
							<tr>
								<th colspan="2">Wednesday, August 3</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>12.00</td>
								<td>Open Singles Routines Semifinals</td>
							</tr>
							<tr>
								<td>14.30</td>
								<td>Women’s Circle Contest Finals</td>
							</tr>
							<tr>
								<td>16.00</td>
								<td>Open Circle Contest Semifinals</td>
							</tr>
							<tr>
								<td>17.30</td>
								<td>Sick 3</td>
							</tr>
							</tbody>
							<thead>
							<tr>
								<th colspan="2">Thursday, August 4</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>15:30</td>
								<td>Open & Intermediate Request Contest</td>
							</tr>
							<tr>
								<td>17:00</td>
								<td>Intermediate Singles Routines Finals</td>
							</tr>
							</tbody>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane" id="net-events">
						<h3>Net events</h3>
						<table class="table table-hover">
							<thead>
								<tr>
									<th colspan="2">Sunday, July 31</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>11:00</td>
									<td>Open Singles Pools</td>
								</tr>
								<tr>
									<td>15:15</td>
									<td>Mixed and Intermediate Players’ Meetings</td>
								</tr>
								<tr>
									<td>15:30</td>
									<td>Mixed Doubles Pools</td>
								</tr>
								<tr>
									<td>15:30</td>
									<td>Intermediate Singles Pools</td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Monday, August 1</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>10:30</td>
									<td>Open Singles Brackets</td>
								</tr>
								<tr>
									<td>12:15</td>
									<td>Women’s Singles Meeting</td>
								</tr>
								<tr>
									<td>12:30</td>
									<td>Women’s Singles Pools</td>
								</tr>
								<tr>
									<td>12:30</td>
									<td>Open Singles Brackets</td>
								</tr>
								<tr>
									<td>13:30</td>
									<td>Intermediate Singles Brackets</td>
								</tr>
								<tr>
									<td>15:00</td>
									<td>Open Doubles Players’ Meeting</td>
								</tr>
								<tr>
									<td>15:15</td>
									<td>Open Doubles Pools</td>
								</tr>
								<tr>
									<td>15:15</td>
									<td>Intermediate Players’ Meeting</td>
								</tr>
								<tr>
									<td>15:30</td>
									<td>Intermediate Doubles Pools</td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Tuesday, August 2</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>11:00</td>
									<td>Open Singles Round of 16</td>
								</tr>
								<tr>
									<td>12:00</td>
									<td>Women’s Singles Round of 8 / Intermediate Singles Semifinals</td>
								</tr>
								<tr>
									<td>13:00</td>
									<td>Women’s Singles Semifinals / Intermediate Doubles Semifinals</td>
								</tr>
								<tr>
									<td>14:00</td>
									<td>Open Doubles Round of 32</td>
								</tr>
								<tr>
									<td>14:45</td>
									<td>Women’s Doubles Players’ Meeting</td>
								</tr>
								<tr>
									<td>15:00</td>
									<td>Women’s Doubles Pools</td>
								</tr>
								<tr>
									<td>17:00</td>
									<td>Open Doubles Round of 16</td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Wednesday, August 3</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>10:00</td>
									<td>Mixed Doubles Round of 16 and Round of 8</td>
								</tr>
								<tr>
									<td>12.00</td>
									<td>Intermediate Singles Bronze Match & Finals</td>
								</tr>
								<tr>
									<td>15.00</td>
									<td>Intermediate Doubles Bronze Match & Finals</td>
								</tr>
								<tr>
									<td>16.00</td>
									<td>Open Singles Quarterfinals</td>
								</tr>
								<tr>
									<td>19.00</td>
									<td>Mixed Doubles Semifinals</td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Thursday, August 4</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>11.45</td>
									<td>Women’s Singles Bronze Match</td>
								</tr>
								<tr>
									<td>12.30</td>
									<td>Open Doubles Quarterfinals</td>
								</tr>
								<tr>
									<td>15.00</td>
									<td>Women’s Doubles Bronze Match</td>
								</tr>
								<tr>
									<td>18.30</td>
									<td>Mixed Doubles Bronze Match</td>
								</tr>
								<tr>
									<td>19.15</td>
									<td>Open Singles Semifinals</td>
								</tr>
							</tbody>
						</table>

					</div>
					<div role="tabpanel" class="tab-pane" id="finals">
						<h3>Finals (Freestyle and Net)</h3>
						<table class="table table-hover">
							<thead>
								<tr>
									<th colspan="2">Friday, August 5</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>12:30</td>
									<td>Two-Square Finals</td>
								</tr>
								<tr>
									<td>13:00</td>
									<td>Women’s Singles Net Finals</td>
								</tr>
								<tr>
									<td>14:00</td>
									<td>Open Singles Net Bronze Match</td>
								</tr>
								<tr>
									<td>15:00</td>
									<td>Open Doubles Net Semifinals 1</td>
								</tr>
								<tr>
									<td>16:30</td>
									<td>Open Doubles Net Semifinals 2</td>
								</tr>
								<tr>
									<td>17:45</td>
									<td>Commercial Show – Last Man Standing</td>
								</tr>
								<tr>
									<td>18:00</td>
									<td>Mixed Doubles Routines Finals</td>
								</tr>
								<tr>
									<td>18:15</td>
									<td>Open Shred 30 Finals</td>
								</tr>
								<tr>
									<td>18:30</td>
									<td>Open Circle Contest Finals</td>
								</tr>
								<tr>
									<td>19:00</td>
									<td>Open Singles Net Finals</td>
								</tr>
								<tr>
									<td>20:00</td>
									<td>Award Ceremony</td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Saturday, August 6</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>12:00</td>
									<td>Women’s Doubles Net Finals</td>
								</tr>
								<tr>
									<td>13:30</td>
									<td>Open Doubles Net Bronze Match</td>
								</tr>
								<tr>
									<td>15:00</td>
									<td>Mixed Doubles Net Finals</td>
								</tr>
								<tr>
									<td>16:30</td>
									<td>Open Doubles Routines Finals</td>
								</tr>
								<tr>
									<td>17:00</td>
									<td>Women’s Singles Routines Finals</td>
								</tr>
								<tr>
									<td>17:30</td>
									<td>Open Singles Routines Finals</td>
								</tr>
								<tr>
									<td>18:00</td>
									<td>Commercial Show – Highest Spike</td>
								</tr>
								<tr>
									<td>18:15</td>
									<td>Open Doubles Net Finals</td>
								</tr>
								<tr>
									<td>19:45</td>
									<td>Award Ceremony</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane" id="other-events">
						<h3>Other events</h3>
						<table class="table table-hover">
							<thead>
								<tr>
									<th colspan="2">Sunday, July 31</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>20:00</td>
									<td>Opening Ceremony</td>
							</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Monday, August 1</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>9:00</td>
									<td>Tournament Late Check-In</td>
								</tr>
								<tr>
									<td>20:00</td>
									<td>IFPA Board of Directors Meeting</td>
								</tr>
								<tr>
									<td>20:30</td>
									<td>IFC Meeting</td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Wednesday, August 2</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>20:00</td>
									<td>EFC Meeting</td>
								</tr>
							</tbody>
							<thead>
								<tr>
									<th colspan="2">Thursday, August 3</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>10:00</td>
									<td>Open Golf</td>
								</tr>
								<tr>
									<td>12:00</td>
									<td>Two-Square Competition (through semifinals)</td>
								</tr>
								<tr>
									<td>20:00</td>
									<td>Small Award Ceremony (all finished competitions)</td>
								</tr>
								<tr>
									<td>21:00</td>
									<td>Hall of Fame Ceremony (Players’ Zone)</td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>

			</div>
		</div>
	</div>
</div>
