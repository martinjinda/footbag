<div id="content" class="container-fluid">
  <div id="judging-criteria" class="container">
    <h2 class="main-headline">Judging criteria</h2>
    <div class="row">
      <div id="city" class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <p>The final score of a video in the contest will be made up of the judges’ decision and spectators’ preference. The weight of both will be derived from the number of votes gathered from the audience, hence will likely not be equal.</p>
        <p>All judges will be obliged to explain their scoring when asked. While the votes from the spectators will be based purely on their preference, the judges shall follow following criteria.</p>
        <h3>Points will be awarded for:</h3>
        <ul>
          <li><strong>Originality/Creativity</strong>. Any attempts at creating a unique video is seen as positive.</li>
          <li><strong>Content. Self explanatory</strong>. If the judges like the concept and the content of your video, you will get a high score. Hint: Judges like footbag.</li>
          <li><strong>Cleanliness/Form</strong>. Any shred in your videos will be assessed on the cleanliness of the tricks and good form/posture while playing. The difficulty and intensity will not be ignored either and may especially help you get the audience on your side.</li>
          <li><strong>Showcasing possibilities</strong>. Could your video be used to promote our sport? Do the players express positive emotions? Is the bag visible? Are you sure? Really? If not, get some big balls from any of the many renowned footbag stitchers.</li>
          <li><strong>Camera/Edit</strong>. A fair amount of points will be granted for good camera work and sharp editing skills.</li>
        </ul>
       <h3>Points will be deducted for:</h3>
        <ul>
          <li>Lack of flow and Music/Shred synergy. While you can do triple dexes to Enya and Tiltless to Napalm Death, it is discouraged and not advised.</li>
          <li>Plagiarism. You can inspire yourself by the work of other, just try not to go overboard.</li>
          <li>Showing footbag in a bad light. We may all be lunatics, but let’s keep it to ourselves. :-)</li>
        </ul>
        <p>We reserve all rights to not include any submitted video that should be found inappropriate or in any way offensive in the contest.</p>
        <h3>Submissions</h3>
        <p>Submit your videos at <a href="mailto:janousek315@gmail.com">janousek315@gmail.com</a>. If they are too large to send by email, either upload them to an international cloud (Dropbox, for example) or upload them on Youtube as unlisted. Do not release your videos or show them to others before they are revealed at the contest venue.</p>
        <p>If you have any questions about the contest or anything to discuss about the rules, please contact me either by email or Facebook.</p>
        <p>Thank you and have a good day.<br/>
        Vojta</p>
      </div>
  </div>
</div>