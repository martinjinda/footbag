<div id="content" class="container-fluid">
  <div id="party-time" class="container">
    <h2 class="main-headline">Party Time</h2>
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <p>Official evening events will be posted later. In the meantime, check out some places recommended by our team:</p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-12 photo-block map-block">
        <iframe src="https://www.google.com/maps/d/embed?mid=1uEr75nojmZfnZhhTmTrNTb1iw28" height="360"></iframe>
      </div>
    </div>
  </div>
</div>