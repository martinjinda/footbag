<div id="content" class="container-fluid">
	<div id="accommodation" class="container">
		<h2 class="main-headline">Accommodation</h2>

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h3>Reservations</h3>
				<p>All accommodation reservations will be made through <a href="mailto:mailto:trnava16accomodation@gmail.com">Viatcheslav Sidorin</a>. You will also be requested to contact him once you register for the event if you want to use Special Worlds Accommodation Packages.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h4>Empire Hotel ****</h4>
				<p>With this premium option, you will stay in a single or double-bed (or twin-bed) rooms, enjoy breakfast overlooking the Net Hall and relax in their private wellness and spa. All rooms include flat-screen TV, air conditioning, private bathroom, and free wifi. You can literally walk down to lobby from your room and be in the Net Hall. As this is a small hotel, only 15 rooms are available to us. Breakfast and wellness/spa are included in the Footbag Worlds Special Deal Package.</p>
				<ul class="list list-inline">
					<li><a href="http://hotelempire.sk/hotel/" target="_blank">www.hotelempire.sk/hotel/</a></li>
				</ul>
			</div>
			<div class="col-sm-12 col-md-12 photo-block">
				<img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/empire-hotel.jpg" alt="Empire Hotel ****">
			</div>
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<address>
					<strong>Address:</strong> Jána Hajdóczyho 7736/11, 917 01 Trnava, Slovensko
				</address>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h5>Special worlds deal</h5>
				<ul class="list list-normal no-margin-top">
					<li><strong>Single:</strong> 53€ per room per night</li>
					<li><strong>Double:</strong> 58€ per room per night (double bed or twin bed possible)</li>
				</ul>
				<p>Breakfast, Hotel Wellness and Spa, VAT, and City tax per person per night included in price.</p>
				<p><strong>Note:</strong> We have only 15 rooms reserved in this hotel. Please reach out to Viatcheslav Sidorin to secure your room in advance.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h4>Holiday Inn</h4>
				<p>As we know there isn’t enough room in the Empire Hotel, we will have a special deal in Holiday Inn as well. We are currently negotiating a special worlds deal—if you are interested in this option, please email <a href="mailto:mailto:trnava16accomodation@gmail.com">Viatcheslav Sidorin</a>. Once we get a price, we will notify you and confirm your reservation.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h4>Student rooms</h4>
				<p>This budget accommodation option has rooms with two beds and a private bathroom—newly reconstructed in 2015, with more than 300 beds available. Located right next to the City Sport Hall, you can see it from the room windows.</p>
			</div>
			<div class="col-sm-12 col-md-12 photo-block">
				<img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/student-rooms.jpg">
			</div>
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<address>
					<strong>Address:</strong> Rybníková 15, 917 01 Trnava, Slovensko
				</address>
				<h5>Special worlds deal</h5>
				<p><strong>Double:</strong> We are currently negotiating the pricing, but expect to pay around 10€ per person per night.</p>
			</div>
		</div>
		
	</div>
</div>