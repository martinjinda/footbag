<div id="content" class="container-fluid">
	<div class="container">
		<h2 class="main-headline">IFPA Worlds coming to Trnava</h2>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<p>On behalf of the worlds organizing crew, IFPA, and the city of Trnava, we would like to invite all players, spectators, and friends to join us this year at the greatest annual gathering of our wonderful sport. On the week of 30th July to 6th August, the best footbag athletes will battle for titles in both freestyle and net disciplines. City of Trnava and its Mayor Peter Bročka are happy to host this event for the first time in Slovakia.</p>
				<p><a target="_blank" href="https://www.facebook.com/footbagworlds/">Follow us on Facebook</a></p>
			</div>
			<div class="col-sm-12 col-md-12 photo-block">
				<img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/trnava-kostel.jpg" alt="IFPA Worlds coming to Trnava">
			</div>
		</div>
	</div>
</div>