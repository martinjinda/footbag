<div id="content" class="container-fluid">
  <div id="city" class="container">
    <h2 class="main-headline">City</h2>
    <div class="row">
      <div id="city" class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <h3>Trnava</h3>
        <p>Trnava is one of the oldest and most important cities in Slovakia. It is a natural centre of lower Považie, the seat of the Trnava district and Trnava Self-Governing region. With the population of 65 000 inhabitants it is the seventh biggest city in Slovakia.</p>
        <p>Find out more about the city at <a target="_blank" href="http://www.trnava.sk/en">www.trnava.sk/en</a> or <a target="_blank" href="https://en.wikipedia.org/wiki/Trnava">wikipedia.org/wiki/Trnava</a></p>
      </div>
      <div class="col-sm-12 col-md-12 photo-block">
        <img src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/photos/trnava-city.jpg" alt="IFPA Worlds coming to Trnava">
      </div>
      <div id="directions" class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <h3>Directions</h3>
        <h4>Plane</h4>
        <p>Bratislava Airport (IATA: BTS) is the nearest commercial airport located about 40 km southward. Vienna International Airport (IATA: VIE) is another airport within reach. Flights to Bratislava or Vienna are the best option.</p>
        <h5>Special airport shuttle service</h5>
        <p>We will have our own shuttle service from Friday to Sunday on the arrival dates from these airports for players to use in certain times. More information will be announced later.</p>
        <h4>Train</h4>
        <p>Trnava lies on a major Slovak train route from Bratislava to Kosice, which provides an easy railway access from many Slovak cities. Train travel from Bratislava takes only about 30 minutes. There are another two railway lines stemming from Trnava: one with westward direction (Smolenice, Kuty) and another leading Southeast to town of Sered. Trnava has direct railway connections with these European capitals: Vienna, Warsaw, and Kiev.</p>
        <h4>Bus</h4>
        <p>Trnava is located on a major D1 Slovak freeway and close to its R1 intersection. It has a good connection to Slovak capital Bratislava and nearby cities such as Nitra, Piestany, or Trencin. Bus station is located near Trnava city center, next to a railway station. There are buses leaving to/arriving from Bratislava at least once an hour, more frequently in peak hours. Quite many people commute from Trnava to Bratislava and also from nearby villages to Trnava. Bus travel from Bratislava to Trnava takes about 50 minutes. There are direct bus lines to some major European cities, such as Prague, Vienna, and Munich.</p>
    </div>
    </div>
  </div>
</div>