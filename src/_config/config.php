<?php
	/**
	 * Created by PhpStorm.
	 * User: martinjinda
	 * Date: 12/04/16
	 * Time: 13:41
	 */

	define('MODE_ENV', 'PROD');

	$config = [
		'DEV' => [
			'BASE_URL' => 'http://footbag.local:8888'
		],
		'TEST' => [
			'BASE_URL' => 'http://jinda.cz/footbag'
		],
		'PROD' => [
			'BASE_URL' => 'http://jinda.cz/footbag'
		]
	];

	$pages = [
		'players' => [
			'title' => 'Players'
		],
		'visitors' => [
			'title' => 'Visitors'
		],
		'media' => [
			'title' => 'Media'
		],
		'sponsors' => [
			'title' => 'Sponsors'
		],
		'accommodation' => [
			'title' => 'Accommodation'
		],
		'city' => [
			'title' => 'City'
		],
		'disciplines' => [
			'title' => 'Disciplines'
		],
		'event-team' => [
			'title' => 'Event Team'
		],
		'hall-of-fame' => [
			'title' => 'Hall of Fame'
		],
		'judging-criteria' => [
			'title' => 'Judging Criteria'
		],
		'party-time' => [
			'title' => 'Party Time'
		],
		'schedule' => [
			'title' => 'Schedule'
		],
		'venues' => [
			'title' => 'Venues & Maps'
		],
		'video-contest' => [
			'title' => 'Video Contest'
		]
	];

	$menu_active = [
		'players' => '',
		'visitors' => '',
		'media' => '',
		'sponsors' => '',
		'accommodation' => '',
		'city' => '',
		'disciplines' => '',
		'event-team' => '',
		'hall-of-fame' => '',
		'judging-criteria' => '',
		'party-time' => '',
		'schedule' => '',
		'venues' => '',
		'video-contest' => ''
	];
	$menu_active[$_GET['page']] = ' class="active"';


