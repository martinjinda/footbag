<div id="pre-footer" class="sub-page container-fluid">
	<div class="container">
		<?php if( strpos($_SERVER['PHP_SELF'], 'players') ) { ?>
			<a class="btn" target="_blank" href="#" title="Registration coming soon">Registration coming soon</a>
			<p>Registration will be available soon at footbag.org.</p>
		<?php } else { ?>
			<a class="btn" target="_blank" href="https://www.facebook.com/footbagworlds/" title="Follow us on FB">Follow us on Facebook</a>
			<p><a target="_blank" href="https://www.facebook.com/footbagworlds/">Visit our Facebook page</a> to check all important updates, photos, and results.</p>
		<?php } ?>
	</div>
</div>

<div id="footer" class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-md-3">
				<h2>Players</h2>
				<ul>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/city" title="City &amp; directions">City &amp; directions</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/venues" title="Venues &amp; maps">Venues &amp; maps</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/accommodation" title="Accommodation">Accommodation</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/event-team" title="Event team">Event team</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/schedule" title="Schedule">Schedule</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/party-time" title="Party time">Party time</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/hall-of-fame" title="Hall of Fame">Hall of Fame</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/video-contest" title="Video contest">Video contest</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-md-3">
				<h2>Visitors</h2>
				<ul>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/disciplines" title="Footbag disciplines">Footbag disciplines</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/city" title="City &amp; directions">City &amp; directions</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/venues" title="Venues &amp; maps">Venues &amp; maps</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/schedule" title="Schedule">Schedule</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/party-time" title="Party time">Party time</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/hall-of-fame" title="Hall of Fame">Hall of Fame</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/video-contest" title="Video contest">Video contest</a></li>
				</ul>
			</div>
			<div class="col-sm-3 col-md-3">
				<h2>Contact</h2>
				<ul>
					<li><a target="_blank" href="https://www.facebook.com/footbagworlds/" title="Facebook">Facebook</a></li>
					<li><a href="mailto:jan.struz@gmail.com" title="Email">Email</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

</body>
</html>