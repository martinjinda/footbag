<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge" />

	<?php if(isset($_GET["page"])) { ?>
			<title><?php echo $pages[$_GET["page"]]['title'] ?> | Footbag Worlds 2016 in Trnava</title>
	<?php } elseif(strpos($_SERVER['PHP_SELF'], 'visitors')) { ?>
		<title>Visitors | Footbag Worlds 2016 in Trnava</title>
	<?php } elseif(strpos($_SERVER['PHP_SELF'], 'players')) { ?>
		<title>Players | Footbag Worlds 2016 in Trnava</title>
	<?php } else { ?>
		<title>Footbag Worlds 2016 in Trnava</title>
	<?php } ?>

	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/css/main.css" media="screen, projection" rel="stylesheet" type="text/css" />
	<link href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/css/print.css" media="print" rel="stylesheet" type="text/css" />
	<!--[if IE]>
	<link href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/css/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
	<![endif]-->

	<script src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/js/jquery-2.2.3.min.js"></script>
	<script src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/js/main.js"></script>

</head>
<body>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<?php if( isset($_GET['page']) or strpos($_SERVER['PHP_SELF'], 'visitors') or strpos($_SERVER['PHP_SELF'], 'players')) { ?>

	<div id="header" class="sub-page container-fluid">
		<div class="container">
			<div class="nav-toggle">Menu</div>
			<nav>
				<ul>
					<li><a <?php if(strpos($_SERVER['PHP_SELF'], 'players')) { echo 'class="active"'; } ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players" title="Players">Players</a></li>
					<li><a <?php if(strpos($_SERVER['PHP_SELF'], 'visitors')) { echo 'class="active"'; } ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors" title="Visitors">Visitors</a></li>
					<li class="nav-logo"><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>"><span class="logo"><svg class="icon-logo"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-logo"></use></svg></span></a></li>
					<li><a <?php echo $menu_active['media']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/media" title="Media">Media</a></li>
					<li><a <?php echo $menu_active['sponsors']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/sponsors" title="Sponsors">Sponsors</a></li>
				</ul>
			</nav>
		</div>
	</div>

	<?php
		$file = dirname( $_SERVER['SCRIPT_FILENAME'] ) . "/" . $_GET['page']. ".php";
		if ( file_exists( $file ) ){
			if( $_GET['page'] == 'visitors') {
				include_once 'sub-visitors.php';
			} elseif( $_GET['page'] == 'players') {
				include_once 'sub-players.php';
			}
		}
	?>

<?php } else { ?>

	<div id="header" class="homepage container-fluid">
		<div class="container">
			<div class="nav-toggle">Menu</div>
			<nav>
				<ul>
					<li><a <?php echo $menu_active['players']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players" title="Players">Players</a></li>
					<li><a <?php echo $menu_active['visitors']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors" title="Visitors">Visitors</a></li>
					<li><a <?php echo $menu_active['media']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/media" title="Media">Media</a></li>
					<li><a <?php echo $menu_active['sponsors']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/sponsors" title="Sponsors">Sponsors</a></li>
				</ul>
			</nav>
			<h1>Footbag<span class="logo hidden-xs"><svg class="icon-logo"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-logo"></use></svg></span> <span class="u-txt-yellow u-strong-underline">worlds</span></h1>
			<h2><strong>30 July–6 Aug 2016</strong> in Trnava, Slovakia</h2>
		</div>
	</div>

<?php } ?>