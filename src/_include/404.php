<div id="content" class="container-fluid">
	<div class="container">
		<h2 class="main-headline">Page not found</h2>
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h3>This isn’t the page you’re looking for.</h3>
				<p>We can’t find <strong>/<?php echo $_GET['page'] ?></strong>. Please use the navigation below.</p>
				<ul class="list list-inline">
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players" title="Players">Players</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors" title="Visitors">Visitors</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/media" title="Media">Media</a></li>
					<li><a href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/sponsors" title="Sponsors">Sponsors</a></li>
				</ul>
			</div>

		</div>
	</div>
</div>