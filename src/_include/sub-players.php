<div id="sub-nav" class="container-fluid">
	<div class="container">
		<div class="secondary-menu">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="row left-menu">
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['city']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/city#city" title="City &amp; directions">City &amp; directions <svg class="icon-wall hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-wall"></use></svg></a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['venues']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/venues#venues-maps" title="Venues &amp; maps">Venues &amp; maps <svg class="icon-labels hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-labels"></use></svg></a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['accommodation']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/accommodation#accommodation" title="Accommodation">Accommodation <svg class="icon-luggage hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-luggage"></use></svg></a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['event-team']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/event-team#event-team" title="Event team">Event team <svg class="icon-policeman hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-policeman"></use></svg></a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="row right-menu">
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['schedule']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/schedule#schedule" title="Schedule"><svg class="icon-alarm hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-alarm"></use></svg> Schedule</a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['party-time']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/party-time#party-time" title="Party time"><svg class="icon-beer hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-beer"></use></svg> Party time</a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['hall-of-fame']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/hall-of-fame#hall-of-fame" title="Hall of Fame"><svg class="icon-cup hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-cup"></use></svg> Hall of Fame</a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['video-contest']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/players/video-contest#video-contest" title="Video contest"><svg class="icon-film hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-film"></use></svg> Video contest</a>
						</div>
					</div>
				</div>
			</div>
		</div>
<!--		<a class="btn" target="_blank" href="http://www.footbag.org/members/join" title="Register now">Register now</a>-->
		<a class="btn" target="_blank" href="#" title="Registration coming soon">Registration coming soon</a>
	</div>
</div>