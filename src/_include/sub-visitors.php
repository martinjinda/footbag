<div id="sub-nav" class="container-fluid">
	<div class="container">
		<div class="secondary-menu">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="row left-menu">
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['disciplines']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/disciplines#disciplines" title="Footbag disciplines">Footbag disciplines<svg class="icon-wall hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-wall"></use></svg></a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['venues']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/venues#venues-maps" title="Venues &amp; maps">Venues &amp; maps <svg class="icon-labels hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-labels"></use></svg></a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['event-team']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/event-team#event-team" title="Event team">Event team <svg class="icon-policeman hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-policeman"></use></svg></a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="row right-menu">
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['schedule']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/schedule#schedule" title="Schedule"><svg class="icon-alarm hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-alarm"></use></svg> Schedule</a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['party-time']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/party-time#party-time" title="Party time"><svg class="icon-beer hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-beer"></use></svg> Party time</a>
						</div>
						<div class="col-xs-6 col-sm-12 col-md-12">
							<a <?php echo $menu_active['video-contest']; ?> href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/visitors/video-contest#video-contest" title="Video contest"><svg class="icon-film hidden-xs"><use xlink:href="<?php echo $config[MODE_ENV]['BASE_URL']; ?>/img/icons.svg#icon-film"></use></svg> Video contest</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>