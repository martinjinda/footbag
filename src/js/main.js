$(function(){
	var url = document.location.toString();
	if (url.match('#')) {
		$('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
	}

	// Change hash for page-reload
	$('.nav-tabs a').on('shown.bs.tab', function (e) {
		window.location.hash = e.target.hash;
	});

});

$(function () {

	var navToggle = $('.nav-toggle');

	if(navToggle.hasClass('active')) {
		$('body').css('overflow', 'hidden');
	}

	navToggle.click(function(e) {
		e.stopPropagation();
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).next().hide();
		} else {
			$(this).next().toggle();
			$(this).toggleClass('active');
		}
	});
});