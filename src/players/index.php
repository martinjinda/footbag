<?php
  /**
   * Created by PhpStorm.
   * User: martinjinda
   * Date: 12/04/16
   * Time: 13:35
   */

  // error_reporting(E_ALL);
  // ini_set('display_errors', true);

  require_once '../_config/config.php';
  require_once '../_include/header.php';
  require_once '../_include/sub-players.php';

  if (isset($_GET["page"])):
    $soubor = $_GET["page"];
    $soubor2 = dirname($_SERVER['SCRIPT_FILENAME'])."/".$soubor.".php";
    if (file_exists($soubor2)):
      if (substr_count($soubor,"../")>0):
        echo "Nelze nahrát adresář v nadřazené složce!";
      elseif ($soubor=="index" or $soubor=="/index"):
        echo "Index nemůže načíst sám sebe!";
      else:
        include $soubor2;
      endif;
    else:
      include '../_include/404.php';
    endif;
  else:
    include '../content/city.php';
  endif;

  require_once "../_include/footer.php";

?>